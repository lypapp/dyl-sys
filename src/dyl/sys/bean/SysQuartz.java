/**
 * 由 EasyCode自动生成
 * @author Dyl
 * 2017-05-25 16:04:28
*/
package dyl.sys.bean;

import java.io.Serializable;
import com.alibaba.fastjson.annotation.JSONField;
import java.math.BigDecimal;
import java.util.Date;

public class SysQuartz implements Serializable {
    private static final long serialVersionUID = 1L;
	/*字段说明：
	*对应db字段名:id 类型:NUMBER(22) 主键 
	*是否可以为空:是
	*/
	
	private  BigDecimal  id;
	
	/*字段说明：触发器名称
	*对应db字段名:triggername 类型:VARCHAR2(40)  
	*是否可以为空:是
	*/
	
	private  String  triggername;
	
	/*字段说明：时间表达式
	*对应db字段名:cronexpression 类型:VARCHAR2(40)  
	*是否可以为空:是
	*/
	
	private  String  cronexpression;
	
	/*字段说明：脚本名称
	*对应db字段名:jobdetailname 类型:VARCHAR2(40)  
	*是否可以为空:是
	*/
	
	private  String  jobdetailname;
	
	/*字段说明：目标类
	*对应db字段名:targetobject 类型:VARCHAR2(40)  
	*是否可以为空:是
	*/
	
	private  String  targetobject;
	
	/*字段说明：方法名
	*对应db字段名:methodname 类型:VARCHAR2(40)  
	*是否可以为空:是
	*/
	
	private  String  methodname;
	
	/*字段说明：是否并发启动任务
	*对应db字段名:concurrent 类型:NUMBER(22)  
	*是否可以为空:是
	*/
	
	private  BigDecimal  concurrent;
	
	/*字段说明：0:关闭,1启用
	*对应db字段名:state 类型:NUMBER(22)  
	*是否可以为空:是
	*/
	
	private  BigDecimal  state;
	
	/*字段说明：
	*对应db字段名:create_date 类型:DATE(7)  
	*是否可以为空:是
	*/
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private  Date  createDate;
	
    public BigDecimal getId() {
        return id;
    }
    public void setId(BigDecimal id) {
        this.id = id;
    }
    public String getTriggername() {
        return triggername;
    }
    public void setTriggername(String triggername) {
        this.triggername = triggername;
    }
    public String getCronexpression() {
        return cronexpression;
    }
    public void setCronexpression(String cronexpression) {
        this.cronexpression = cronexpression;
    }
    public String getJobdetailname() {
        return jobdetailname;
    }
    public void setJobdetailname(String jobdetailname) {
        this.jobdetailname = jobdetailname;
    }
    public String getTargetobject() {
        return targetobject;
    }
    public void setTargetobject(String targetobject) {
        this.targetobject = targetobject;
    }
    public String getMethodname() {
        return methodname;
    }
    public void setMethodname(String methodname) {
        this.methodname = methodname;
    }
    public BigDecimal getConcurrent() {
        return concurrent;
    }
    public void setConcurrent(BigDecimal concurrent) {
        this.concurrent = concurrent;
    }
    public BigDecimal getState() {
        return state;
    }
    public void setState(BigDecimal state) {
        this.state = state;
    }
    public Date getCreateDate() {
        return createDate;
    }
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
	public String toString() {
		return 
		"id:"+id+"\n"+
		
		"triggername:"+triggername+"\n"+
		
		"cronexpression:"+cronexpression+"\n"+
		
		"jobdetailname:"+jobdetailname+"\n"+
		
		"targetobject:"+targetobject+"\n"+
		
		"methodname:"+methodname+"\n"+
		
		"concurrent:"+concurrent+"\n"+
		
		"state:"+state+"\n"+
		
		"createDate:"+createDate+"\n"+
		"";
	}
}
