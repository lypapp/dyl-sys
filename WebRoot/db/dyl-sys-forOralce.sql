﻿prompt PL/SQL Developer import file
prompt Created on 2017年6月22日 by Administrator
set feedback off
set define off
prompt Creating SYS_AUTH_ROLE...
create table SYS_AUTH_ROLE
(
  id          NUMBER,
  role_id     NUMBER,
  menu_id     NUMBER,
  kind_id     NUMBER,
  create_time DATE default sysdate
)
;
comment on column SYS_AUTH_ROLE.role_id
  is '角色id';
comment on column SYS_AUTH_ROLE.menu_id
  is '菜单id';
comment on column SYS_AUTH_ROLE.kind_id
  is '权限id';
comment on column SYS_AUTH_ROLE.create_time
  is '创建时间';

prompt Creating SYS_FILEINFO...
create table SYS_FILEINFO
(
  id          NUMBER,
  real_name   VARCHAR2(100),
  upload_name VARCHAR2(100),
  file_size   NUMBER,
  file_url    VARCHAR2(200),
  creator     NUMBER,
  create_time DATE default sysdate,
  c_id        NUMBER
)
;
comment on column SYS_FILEINFO.real_name
  is '文件存储的真实名称';
comment on column SYS_FILEINFO.upload_name
  is '上传文件名称';
comment on column SYS_FILEINFO.file_size
  is '文件大小';
comment on column SYS_FILEINFO.file_url
  is '文件下载地址';
comment on column SYS_FILEINFO.creator
  is '上传人';
comment on column SYS_FILEINFO.create_time
  is '上传时间';
comment on column SYS_FILEINFO.c_id
  is '关联的业务id';

prompt Creating SYS_KIND...
create table SYS_KIND
(
  id   NUMBER,
  name VARCHAR2(20)
)
;
comment on column SYS_KIND.id
  is '种类ID';
comment on column SYS_KIND.name
  is '种类名称';

prompt Creating SYS_LOG...
create table SYS_LOG
(
  id        NUMBER(20),
  url       VARCHAR2(255),
  para      VARCHAR2(555),
  loginuser NUMBER(20),
  ip        VARCHAR2(255),
  time      DATE
)
;

prompt Creating SYS_MENU...
create table SYS_MENU
(
  id           NUMBER not null,
  pid          NUMBER,
  oid          NUMBER,
  title        VARCHAR2(50),
  note         VARCHAR2(100),
  state        CHAR(1) default 0,
  url          VARCHAR2(100),
  icon         VARCHAR2(100),
  create_time  DATE default sysdate,
  creator      NUMBER,
  action_class VARCHAR2(50),
  view_level   NUMBER default 5
)
;
comment on column SYS_MENU.pid
  is '父Id';
comment on column SYS_MENU.oid
  is '排序';
comment on column SYS_MENU.title
  is '标题';
comment on column SYS_MENU.note
  is '备注';
comment on column SYS_MENU.state
  is '状态,0正常，1禁用';
comment on column SYS_MENU.url
  is '链接';
comment on column SYS_MENU.icon
  is '图标';
comment on column SYS_MENU.create_time
  is '创建时间';
comment on column SYS_MENU.creator
  is '创建者';
comment on column SYS_MENU.action_class
  is '对应的类';
comment on column SYS_MENU.view_level
  is '等级';
alter table SYS_MENU
  add primary key (ID);

prompt Creating SYS_MENU_KIND...
create table SYS_MENU_KIND
(
  id          NUMBER,
  menu_id     NUMBER,
  kind_id     NUMBER,
  create_time DATE default sysdate
)
;
comment on column SYS_MENU_KIND.menu_id
  is '菜单ID';
comment on column SYS_MENU_KIND.kind_id
  is '权限ID';
comment on column SYS_MENU_KIND.create_time
  is '创建时间';

prompt Creating SYS_QUARTZ...
create table SYS_QUARTZ
(
  id             NUMBER,
  triggername    VARCHAR2(40),
  cronexpression VARCHAR2(40),
  jobdetailname  VARCHAR2(40),
  targetobject   VARCHAR2(40),
  methodname     VARCHAR2(40),
  concurrent     NUMBER default 0,
  state          NUMBER default 0,
  create_date    DATE default sysdate
)
;
comment on column SYS_QUARTZ.triggername
  is '触发器名称';
comment on column SYS_QUARTZ.cronexpression
  is '时间表达式';
comment on column SYS_QUARTZ.jobdetailname
  is '脚本名称';
comment on column SYS_QUARTZ.targetobject
  is '目标类';
comment on column SYS_QUARTZ.methodname
  is '方法名';
comment on column SYS_QUARTZ.concurrent
  is '是否并发启动任务';
comment on column SYS_QUARTZ.state
  is '0:关闭,1启用';

prompt Creating SYS_ROLE...
create table SYS_ROLE
(
  id          NUMBER,
  name        VARCHAR2(200),
  note        VARCHAR2(200),
  creator     NUMBER,
  create_time DATE default sysdate
)
;
comment on column SYS_ROLE.id
  is '主键';
comment on column SYS_ROLE.name
  is '角色名称';
comment on column SYS_ROLE.note
  is '备注';
comment on column SYS_ROLE.creator
  is '创建者Id';
comment on column SYS_ROLE.create_time
  is '创建时间';

prompt Creating SYS_ROLE_USER...
create table SYS_ROLE_USER
(
  id          NUMBER not null,
  roleid      NUMBER,
  userid      NUMBER,
  create_time DATE default sysdate,
  creator     NUMBER
)
;
comment on column SYS_ROLE_USER.id
  is '主键';
comment on column SYS_ROLE_USER.roleid
  is '角色id';
comment on column SYS_ROLE_USER.userid
  is '用户id';
comment on column SYS_ROLE_USER.create_time
  is '创建时间';
comment on column SYS_ROLE_USER.creator
  is '创建人';
alter table SYS_ROLE_USER
  add constraint ID primary key (ID);

prompt Creating SYS_USER...
create table SYS_USER
(
  id          NUMBER not null,
  username    VARCHAR2(50) not null,
  name        VARCHAR2(50),
  password    VARCHAR2(100),
  email       VARCHAR2(100),
  phone       VARCHAR2(100),
  state       CHAR(1) default 0,
  create_time DATE default sysdate,
  creator     NUMBER
)
;
comment on column SYS_USER.username
  is '用户名';
comment on column SYS_USER.name
  is '姓名';
comment on column SYS_USER.password
  is '密码';
comment on column SYS_USER.email
  is '邮箱';
comment on column SYS_USER.phone
  is '电话';
comment on column SYS_USER.state
  is '状态0:正常,1禁用';
comment on column SYS_USER.create_time
  is '创建时间';
comment on column SYS_USER.creator
  is '创建者';

prompt Loading SYS_AUTH_ROLE...
insert into SYS_AUTH_ROLE (id, role_id, menu_id, kind_id, create_time)
values (100024, 2, 2, 1, to_date('26-05-2017 13:44:32', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_AUTH_ROLE (id, role_id, menu_id, kind_id, create_time)
values (100025, 2, 2, 4, to_date('26-05-2017 13:44:32', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_AUTH_ROLE (id, role_id, menu_id, kind_id, create_time)
values (100026, 2, 3, 1, to_date('26-05-2017 13:44:32', 'dd-mm-yyyy hh24:mi:ss'));
commit;
prompt 3 records loaded
prompt Loading SYS_FILEINFO...
insert into SYS_FILEINFO (id, real_name, upload_name, file_size, file_url, creator, create_time, c_id)
values (100397, '201706221547308.docx', 'zhuanhuan.docx', 44889, 'download.do?fileName=201706221547308.docx&name=zhuanhuan.docx', 0, to_date('22-06-2017 15:29:13', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_FILEINFO (id, real_name, upload_name, file_size, file_url, creator, create_time, c_id)
values (100395, '201706221547108.doc', '测试.doc', 114688, 'download.do?fileName=201706221547108.doc&name=测试.doc', 0, to_date('22-06-2017 15:29:13', 'dd-mm-yyyy hh24:mi:ss'), null);
insert into SYS_FILEINFO (id, real_name, upload_name, file_size, file_url, creator, create_time, c_id)
values (100396, '201706221547926.docx', '论文.docx', 158592, 'download.do?fileName=201706221547926.docx&name=论文.docx', 0, to_date('22-06-2017 15:29:13', 'dd-mm-yyyy hh24:mi:ss'), null);
commit;
prompt 3 records loaded
prompt Loading SYS_KIND...
insert into SYS_KIND (id, name)
values (1, '查看');
insert into SYS_KIND (id, name)
values (2, '新增');
insert into SYS_KIND (id, name)
values (3, '修改');
insert into SYS_KIND (id, name)
values (4, '删除');
commit;
prompt 4 records loaded
prompt Loading SYS_LOG...
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100033, '/index.do', null, null, '0:0:0:0:0:0:0:1', to_date('06-06-2017 10:43:15', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100034, '/login!intologin.do', null, null, '0:0:0:0:0:0:0:1', to_date('06-06-2017 10:43:15', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100035, '/login.do', '?username=dev&password=12', null, '0:0:0:0:0:0:0:1', to_date('06-06-2017 10:43:17', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100036, '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', to_date('06-06-2017 10:43:19', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100037, '/index.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 10:43:19', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100038, '/main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 10:43:19', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100039, '/sysMenu!getMenu.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 10:43:19', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100040, '/log!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 10:43:20', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100041, '/easyCode!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 10:43:21', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100042, '/sysUser!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 10:43:27', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100043, '/sysMenu!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 10:43:28', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100044, '/sysMenu!getMenuForZtreeByManage.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 10:43:28', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100045, '/sysMenu!sysMenuForm.do', '?id=8', 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 10:43:29', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100046, '/sysMenu!update.do', '?id=8&icon=#xe621;&title=日志管理&state=0&actionClass=&url=sysLog!main.do&note=', 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 10:43:42', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100047, '/login.do', '?username=dev&password=123', 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 10:43:48', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100048, '/index.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 10:43:48', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100049, '/main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 10:43:48', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100050, '/sysMenu!getMenu.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 10:43:48', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100051, '/sysLog!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 10:43:48', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100052, '/sysQuartz!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 10:43:55', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100053, '/sysQuartz!sysQuartzForm.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 10:43:57', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100054, '/sysQuartz!add.do', '?id=&cronexpression=0/30 * * * * ?&methodname=execute&concurrent=1&state=1&jobdetailname=logInfo&targetobject=logQuarz&triggername=系统访问日志定时器', 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 10:44:46', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100055, '/sysQuartz!main.do', '?currentPage=1&triggername=&totalPage=1', 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 10:44:46', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100230, '/sysLog!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('12-06-2017 15:31:10', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100240, '/login!intologin.do', null, null, '0:0:0:0:0:0:0:1', to_date('21-06-2017 16:49:01', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100241, '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', to_date('21-06-2017 16:49:03', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100242, '/index.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 16:49:03', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100243, '/main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 16:49:03', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100244, '/sysMenu!getMenu.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 16:49:04', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100245, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 16:49:05', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100247, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 16:52:53', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100248, '/index.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 16:52:57', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100249, '/main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 16:52:57', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100250, '/sysMenu!getMenu.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 16:52:57', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100251, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 16:52:59', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100253, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 16:55:20', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100254, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 16:55:22', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100256, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 16:56:15', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100257, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 16:56:21', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100260, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 16:58:34', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100261, '/index.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 16:58:37', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100262, '/main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 16:58:37', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100263, '/sysMenu!getMenu.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 16:58:37', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100264, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 16:58:38', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100265, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 16:58:42', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100266, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 16:58:47', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100267, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 16:58:50', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100269, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:01:16', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100271, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:10:07', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100272, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:10:13', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100273, '/sysFileinfo!main.do', '?uploadName=&currentPage=1&totalPage=1', 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:10:21', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100274, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:11:46', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100275, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:11:51', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100276, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:13:59', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100277, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:14:16', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100278, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:14:45', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100279, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:14:50', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100280, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:15:05', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100281, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:15:08', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100282, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:15:15', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100283, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:15:15', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100284, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:15:15', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100285, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:17:46', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100286, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:18:09', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100287, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:36:39', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100288, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:36:42', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100289, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:36:47', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100290, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:37:00', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100291, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:50:58', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100292, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:51:03', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100293, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:51:11', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100294, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:51:11', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100295, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:51:23', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100296, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:51:23', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100297, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:53:14', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100298, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:53:19', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100299, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:53:19', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100300, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('21-06-2017 17:53:19', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100388, '/login!intologin.do', null, null, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:45:50', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100389, '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:45:53', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100390, '/index.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:45:53', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100391, '/main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:45:53', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100392, '/sysMenu!getMenu.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:45:54', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100393, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:45:55', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100394, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:47:29', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100398, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:47:35', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100399, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:47:35', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100400, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:47:35', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100401, '/sysFileinfo!main.do', '?uploadName=&currentPage=1&totalPage=0', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:47:39', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100402, '/dowload.do', '?fileName=201706221547308.docx&name=zhuanhuan.docx&name=zhuanhuan.docx&fileName=201706221547308.docx', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:47:41', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100403, '/dowload.do', '?fileName=201706221547308.docx&name=zhuanhuan.docx&name=zhuanhuan.docx&fileName=201706221547308.docx', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:47:56', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100404, '/dowload.do', '?fileName=201706221547308.docx&name=zhuanhuan.docx&name=zhuanhuan.docx&fileName=201706221547308.docx', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:48:08', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100405, '/dowload.do', '?fileName=201706221547308.docx&name=zhuanhuan.docx&name=zhuanhuan.docx&fileName=201706221547308.docx', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:48:24', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100406, '/download.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:48:46', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100407, '/download.do', '?fileName=201706221547308.docx&name=zhuanhuan.docx&name=zhuanhuan.docx&fileName=201706221547308.docx', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:48:58', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100408, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:49:34', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100409, '/download.do', '?fileName=201706221547108.doc&name=%E6%B5%8B%E8%AF%95.doc&name=??è?.doc&fileName=201706221547108.doc', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:49:35', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100410, '/download.do', '?fileName=201706221547926.docx&name=%E8%AE%BA%E6%96%87.docx&name=è???.docx&fileName=201706221547926.docx', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:49:36', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100411, '/download.do', '?fileName=201706221547108.doc&name=%E6%B5%8B%E8%AF%95.doc&name=??è?.doc&fileName=201706221547108.doc', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:51:14', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100412, '/download.do', '?fileName=201706221547108.doc&name=%E6%B5%8B%E8%AF%95.doc&name=??è?.doc&fileName=201706221547108.doc', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:52:01', 'dd-mm-yyyy hh24:mi:ss'));
commit;
prompt 100 records committed...
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100413, '/download.do', '?fileName=201706221547926.docx&name=%E8%AE%BA%E6%96%87.docx&name=è???.docx&fileName=201706221547926.docx', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:52:03', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100414, '/download.do', '?fileName=201706221547108.doc&name=%E6%B5%8B%E8%AF%95.doc&name=??è?.doc&fileName=201706221547108.doc', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:52:34', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100415, '/download.do', '?fileName=201706221547926.docx&name=%E8%AE%BA%E6%96%87.docx&name=è???.docx&fileName=201706221547926.docx', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:52:35', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100416, '/sysFileinfo!main.do', '?uploadName=108&currentPage=1&totalPage=1', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:53:01', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100417, '/sysFileinfo!main.do', '?uploadName=108&currentPage=1&totalPage=0', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:53:03', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100418, '/sysFileinfo!main.do', '?uploadName=&currentPage=1&totalPage=0', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:53:05', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100419, '/sysFileinfo!main.do', '?uploadName=??è?&currentPage=1&totalPage=1', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:53:11', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100420, '/sysUser!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:53:16', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100421, '/sysUser!main.do', '?username=??è?&currentPage=1&totalPage=1', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:53:19', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100422, '/sysUser!main.do', '?username=??????¨???&currentPage=1&totalPage=0', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:53:39', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100057, '/sysLog!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 10:45:37', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100058, '/index.do', null, null, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:30:06', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100059, '/login!intologin.do', null, null, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:30:06', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100060, '/login.do', '?username=dev&password=12', null, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:30:07', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100061, '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:30:10', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100062, '/index.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:30:10', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100063, '/main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:30:10', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100064, '/sysMenu!getMenu.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:30:10', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100065, '/sysMenu!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:30:13', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100066, '/sysMenu!getMenuForZtreeByManage.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:30:13', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100067, '/sysMenu!addMenu.do', '?pId=1', 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:30:16', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100068, '/sysMenu!sysMenuForm.do', '?id=1', 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:30:16', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100069, '/sysMenu!sysMenuForm.do', '?id=100056', 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:30:16', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100070, '/sysMenu!sysMenuForm.do', '?id=100056', 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:30:17', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100071, '/sysMenu!update.do', '?id=100056&icon=&title=通用上传&state=0&actionClass=&url=&note=', 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:30:26', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100072, '/easyCode!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:30:28', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100073, '/easyCode!easyCodeForm.do', '?name=SYS_FILEINFO', 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:30:34', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100172, '/index.do', null, null, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:19:11', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100173, '/login!intologin.do', null, null, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:19:12', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100174, '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:19:13', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100175, '/index.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:19:14', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100176, '/main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:19:14', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100177, '/sysMenu!getMenu.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:19:14', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100178, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:19:14', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100179, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:19:17', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100181, '/index.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:48:32', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100182, '/main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:48:32', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100183, '/sysMenu!getMenu.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:48:32', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100184, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:48:33', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100185, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:48:40', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100186, '/sysFileinfo!main.do', '?uploadName=&currentPage=1&totalPage=1', 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:48:46', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100187, '/index.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:49:30', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100188, '/sysMenu!getMenu.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:49:30', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100189, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:01:57', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100190, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:04:08', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100191, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:04:49', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100192, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:05:16', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100193, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:05:25', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100194, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:05:27', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100195, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:05:33', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100196, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:05:38', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100197, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:07:45', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100198, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:07:54', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100199, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:11:28', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100200, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:11:43', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100201, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:12:11', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100202, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:12:21', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100203, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:12:58', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100204, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:13:07', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100205, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:13:34', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100206, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:13:48', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100207, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:14:00', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100208, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:14:07', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100209, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:16:12', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100210, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:17:24', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100211, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:18:07', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100212, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:18:33', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100213, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:19:12', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100214, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:22:15', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100215, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:22:52', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100216, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:25:24', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100217, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:26:38', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100218, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:26:58', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100219, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:27:32', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100220, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:27:40', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100221, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:29:46', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100222, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:31:29', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100223, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:32:24', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100224, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:32:32', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100225, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:33:27', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100226, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:34:47', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100228, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:35:47', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100229, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 17:35:50', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100110, '/easyCode.do', '?name=SYS_FILEINFO&pKey=id&templates=javaBean-layui.ftl%40SysFileinfo.java&templates=javaAction-layui.ftl%40SysFileinfoAction.java&templates=javaServiceImpl-layui.ftl%40SysFileinfoServiceImpl.java&templates=viewMain-layui.ftl%40sysFileinfoMain.jsp&templates=javaBean-layui.ftl@SysFileinfo.java,javaAction-layui.ftl@SysFileinfoAction.java,javaServiceImpl-layui.ftl@SysFileinfoServiceImpl.java,viewMain-layui.ftl@sysFileinfoMain.jsp&name=SYS_FILEINFO&pKey=id', 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:31:41', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100111, '/sysMenu!sysMenuForm.do', '?id=100056', 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:33:19', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100112, '/sysMenu!update.do', '?id=100056&icon=&title=通用上传&state=0&actionClass=&url=sysFileinfo!main.do&note=', 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:33:21', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100113, '/index.do', null, null, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:34:11', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100114, '/login!intologin.do', null, null, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:34:11', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100115, '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:34:15', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100116, '/index.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:34:15', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100117, '/main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:34:15', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100118, '/sysMenu!getMenu.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:34:16', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100119, '/easyCode!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:34:16', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100120, '/sysLog!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:34:17', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100121, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:34:19', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100122, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:34:31', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100123, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:35:04', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100124, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:36:17', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100125, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:37:34', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100126, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:38:46', 'dd-mm-yyyy hh24:mi:ss'));
commit;
prompt 200 records committed...
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100127, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:38:59', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100128, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:40:36', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100129, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:40:55', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100130, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:41:41', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100131, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:48:07', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100132, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:49:18', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100133, '/sysLog!main.do', '?currentPage=1&totalPage=4&url=', 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:50:37', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100134, '/sysLog!main.do', '?currentPage=2&totalPage=6&url=', 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:50:41', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100135, '/sysLog!main.do', '?currentPage=1&totalPage=6&url=', 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:50:42', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100136, '/sysLog!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:50:56', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100137, '/sysMenu!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:51:02', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100138, '/sysMenu!getMenuForZtreeByManage.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:51:02', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100139, '/sysMenu!sysMenuForm.do', '?id=9', 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:51:03', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100140, '/sysMenu!update.do', '?id=9&icon=fa-file-archive-o&title=通用上传&state=0&actionClass=&url=sysFileinfo!main.do&note=', 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:51:54', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100141, '/index.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:51:55', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100142, '/main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:51:55', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100143, '/sysMenu!getMenu.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:51:56', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100144, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:52:02', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100145, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:52:15', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100146, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:52:23', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100147, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:53:11', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100148, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:53:21', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100149, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:53:36', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100150, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:53:38', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100151, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:55:46', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100152, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:55:51', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100153, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:55:57', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100154, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:56:01', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100155, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:56:46', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100156, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:58:48', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100157, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:59:02', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100158, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 15:59:56', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100159, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:00:20', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100160, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:00:43', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100161, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:00:55', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100162, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:00:58', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100163, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:01:46', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100164, '/index.do', null, null, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:17:04', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100165, '/login!intologin.do', null, null, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:17:04', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100166, '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:17:06', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100167, '/index.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:17:06', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100168, '/main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:17:06', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100169, '/sysMenu!getMenu.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:17:06', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100170, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:17:08', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100171, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('06-06-2017 16:17:11', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100301, '/login!intologin.do', null, null, '0:0:0:0:0:0:0:1', to_date('22-06-2017 14:58:31', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100302, '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', to_date('22-06-2017 14:58:33', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100303, '/index.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 14:58:33', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100304, '/main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 14:58:33', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100305, '/sysMenu!getMenu.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 14:58:33', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100306, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 14:59:45', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100307, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 14:59:52', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100308, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 14:59:52', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100309, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:00:04', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100310, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:00:04', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100311, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:00:20', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100312, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:00:25', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100313, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:00:25', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100314, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:01:52', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100315, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:03:02', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100316, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:06:12', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100317, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:06:37', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100318, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:07:30', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100319, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:07:34', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100320, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:07:41', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100321, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:07:42', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100322, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:07:49', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100323, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:07:50', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100324, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:07:51', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100325, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:10:05', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100326, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:10:09', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100327, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:10:09', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100328, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:10:17', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100329, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:10:18', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100330, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:12:16', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100331, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:12:19', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100332, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:12:26', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100333, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:12:26', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100334, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:12:26', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100335, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:16:08', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100336, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:16:12', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100337, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:16:12', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100338, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:16:15', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100339, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:16:18', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100340, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:16:18', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100341, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:16:25', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100342, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:16:28', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100343, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:16:29', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100344, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:16:35', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100345, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:16:36', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100346, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:17:09', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100347, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:17:13', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100348, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:20:36', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100349, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:20:38', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100350, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:20:42', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100351, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:20:47', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100352, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:20:57', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100353, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:20:57', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100354, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:20:58', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100355, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:25:27', 'dd-mm-yyyy hh24:mi:ss'));
commit;
prompt 300 records committed...
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100356, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:25:30', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100357, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:25:34', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100358, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:25:50', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100359, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:25:53', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100360, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:26:53', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100361, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:27:24', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100362, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:27:27', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100363, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:27:33', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100364, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:27:33', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100365, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:27:33', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100366, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:27:41', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100367, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:27:44', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100368, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:27:49', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100369, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:27:49', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100370, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:27:49', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100371, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:29:39', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100372, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:29:44', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100373, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:29:44', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100374, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:31:07', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100375, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:31:11', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100376, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:34:37', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100377, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:34:41', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100378, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:34:41', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100379, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:34:41', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100381, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:35:37', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100382, '/dowload.do', '?real_name=201706211652915.doc&upload_name=zhuanhuan.doc&real_name=201706211652915.doc&upload_name=zhuanhuan.doc', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:35:39', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100383, '/dowload.do', '?real_name=201706061735292.exe&upload_name=SangforCSClient.exe&real_name=201706061735292.exe&upload_name=SangforCSClient.exe', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:35:46', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100384, '/upload.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:35:50', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100385, '/sysFileinfo!main.do', '?uploadName=&currentPage=1&totalPage=1', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:35:53', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100386, '/dowload.do', '?real_name=201706221535831.docx&upload_name=%E8%AE%BA%E6%96%87.docx&real_name=201706221535831.docx&upload_name=è???.docx', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:35:55', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100387, '/dowload.do', '?real_name=201706221535831.docx&upload_name=%E8%AE%BA%E6%96%87.docx&real_name=201706221535831.docx&upload_name=è???.docx', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:43:52', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100423, '/index.do', null, null, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:54:37', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100424, '/login!intologin.do', null, null, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:54:37', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100425, '/login.do', '?username=dev&password=123', null, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:54:39', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100426, '/index.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:54:39', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100427, '/main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:54:39', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100428, '/sysMenu!getMenu.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:54:39', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100429, '/sysUser!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:54:40', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100430, '/sysUser!main.do', '?username=??è?&currentPage=1&totalPage=1', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:54:43', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100431, '/sysUser!main.do', '?username=??????¨???&currentPage=1&totalPage=0', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:56:26', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100432, '/sysUser!main.do', '?username=??è?&currentPage=1&totalPage=0', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:57:08', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100433, '/sysFileinfo!main.do', '?uploadName=??è?&currentPage=1&totalPage=1', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:58:34', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100434, '/sysFileinfo!main.do', '?uploadName=??è?1&currentPage=1&totalPage=0', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 15:59:47', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100435, '/sysFileinfo!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 16:00:24', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100436, '/sysFileinfo!main.do', '?uploadName=&currentPage=1&totalPage=1', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 16:00:25', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100437, '/sysFileinfo!main.do', '?uploadName=测试&currentPage=1&totalPage=1', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 16:00:28', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100438, '/sysLog!main.do', null, 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 16:00:40', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100439, '/download.do', '?fileName=201706221547108.doc&name=%E6%B5%8B%E8%AF%95.doc&name=测试.doc&fileName=201706221547108.doc', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 16:00:43', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100440, '/sysFileinfo!main.do', '?uploadName=测试&currentPage=1&totalPage=1', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 16:01:51', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100441, '/sysFileinfo!main.do', '?uploadName=测试&currentPage=1&totalPage=1', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 16:01:52', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_LOG (id, url, para, loginuser, ip, time)
values (100442, '/download.do', '?fileName=201706221547108.doc&name=%E6%B5%8B%E8%AF%95.doc&name=测试.doc&fileName=201706221547108.doc', 0, '0:0:0:0:0:0:0:1', to_date('22-06-2017 16:02:01', 'dd-mm-yyyy hh24:mi:ss'));
commit;
prompt 351 records loaded
prompt Loading SYS_MENU...
insert into SYS_MENU (id, pid, oid, title, note, state, url, icon, create_time, creator, action_class, view_level)
values (8, 1, 10, '日志管理', null, '0', 'sysLog!main.do', '&#xe621;', to_date('01-06-2017 15:29:40', 'dd-mm-yyyy hh24:mi:ss'), 0, null, 1);
insert into SYS_MENU (id, pid, oid, title, note, state, url, icon, create_time, creator, action_class, view_level)
values (1, 0, 1, '系统管理', null, '0', null, 'fa-cubes', to_date('15-03-2017 15:42:06', 'dd-mm-yyyy hh24:mi:ss'), 0, null, 2);
insert into SYS_MENU (id, pid, oid, title, note, state, url, icon, create_time, creator, action_class, view_level)
values (2, 1, -1, '用户管理', null, '0', 'sysUser!main.do', '&#xe612;', to_date('15-03-2017 15:43:57', 'dd-mm-yyyy hh24:mi:ss'), 0, 'sysUserAction', 2);
insert into SYS_MENU (id, pid, oid, title, note, state, url, icon, create_time, creator, action_class, view_level)
values (3, 1, 0, '菜单管理', null, '0', 'sysMenu!main.do', '&#xe62a;', to_date('15-03-2017 15:43:57', 'dd-mm-yyyy hh24:mi:ss'), 0, null, 1);
insert into SYS_MENU (id, pid, oid, title, note, state, url, icon, create_time, creator, action_class, view_level)
values (4, 1, 6, '角色管理', null, '0', 'sysRole!main.do', '&#xe61b;', to_date('05-04-2017 15:46:44', 'dd-mm-yyyy hh24:mi:ss'), 0, 'sysRoleAction', 2);
insert into SYS_MENU (id, pid, oid, title, note, state, url, icon, create_time, creator, action_class, view_level)
values (9, 1, 11, '通用上传', null, '0', 'sysFileinfo!main.do', 'fa-file-archive-o', to_date('06-06-2017 15:11:38', 'dd-mm-yyyy hh24:mi:ss'), 0, null, 1);
insert into SYS_MENU (id, pid, oid, title, note, state, url, icon, create_time, creator, action_class, view_level)
values (6, 1, 7, '数据库监控', null, '0', 'druid/index.html', '&#xe636;', to_date('24-05-2017 16:13:56', 'dd-mm-yyyy hh24:mi:ss'), 0, null, 1);
insert into SYS_MENU (id, pid, oid, title, note, state, url, icon, create_time, creator, action_class, view_level)
values (5, 1, 9, '代码生成器', null, '0', 'easyCode!main.do', '&#xe62c;', to_date('31-05-2017 14:55:18', 'dd-mm-yyyy hh24:mi:ss'), 0, null, 1);
insert into SYS_MENU (id, pid, oid, title, note, state, url, icon, create_time, creator, action_class, view_level)
values (7, 1, 8, '定时任务管理', null, '0', 'sysQuartz!main.do', '&#xe60a;', to_date('25-05-2017 16:00:25', 'dd-mm-yyyy hh24:mi:ss'), 0, null, 1);
commit;
prompt 9 records loaded
prompt Loading SYS_MENU_KIND...
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (206, 87, 1, to_date('20-04-2017 14:11:21', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (207, 87, 2, to_date('20-04-2017 14:11:21', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (192, 4, 1, to_date('20-04-2017 10:12:27', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (193, 4, 2, to_date('20-04-2017 10:12:27', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (186, 2, 1, to_date('20-04-2017 10:12:14', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (187, 2, 2, to_date('20-04-2017 10:12:14', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (188, 2, 3, to_date('20-04-2017 10:12:14', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (189, 2, 4, to_date('20-04-2017 10:12:14', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (194, 4, 3, to_date('20-04-2017 10:12:27', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (195, 4, 4, to_date('20-04-2017 10:12:27', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (190, 3, 1, to_date('20-04-2017 10:12:20', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_MENU_KIND (id, menu_id, kind_id, create_time)
values (191, 3, 2, to_date('20-04-2017 10:12:20', 'dd-mm-yyyy hh24:mi:ss'));
commit;
prompt 12 records loaded
prompt Loading SYS_QUARTZ...
insert into SYS_QUARTZ (id, triggername, cronexpression, jobdetailname, targetobject, methodname, concurrent, state, create_date)
values (1, '测试1', '0/3 * * * * ?', 'detailname', 'dyl.sys.quartz.TestQuarz', 'haha', 1, 0, to_date('25-05-2017 16:02:56', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_QUARTZ (id, triggername, cronexpression, jobdetailname, targetobject, methodname, concurrent, state, create_date)
values (100032, '系统访问日志定时器', '0/30 * * * * ?', 'logInfo', 'logQuarz', 'execute', 1, 1, to_date('06-06-2017 10:26:08', 'dd-mm-yyyy hh24:mi:ss'));
commit;
prompt 2 records loaded
prompt Loading SYS_ROLE...
insert into SYS_ROLE (id, name, note, creator, create_time)
values (1, '超级管理员', '超级管理员', 1, to_date('06-04-2017 14:11:49', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_ROLE (id, name, note, creator, create_time)
values (2, '测试角色', '测试角色', 1, to_date('20-04-2017 14:11:44', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_ROLE (id, name, note, creator, create_time)
values (0, '开发管理员', '开发管理员最高权限', 1, to_date('20-04-2017 14:26:37', 'dd-mm-yyyy hh24:mi:ss'));
insert into SYS_ROLE (id, name, note, creator, create_time)
values (212, '测试2', '测试21', 2, to_date('20-04-2017 16:34:17', 'dd-mm-yyyy hh24:mi:ss'));
commit;
prompt 4 records loaded
prompt Loading SYS_ROLE_USER...
insert into SYS_ROLE_USER (id, roleid, userid, create_time, creator)
values (209, 0, 0, to_date('20-04-2017 16:33:47', 'dd-mm-yyyy hh24:mi:ss'), 2);
insert into SYS_ROLE_USER (id, roleid, userid, create_time, creator)
values (210, 1, 2, to_date('20-04-2017 16:33:54', 'dd-mm-yyyy hh24:mi:ss'), 2);
insert into SYS_ROLE_USER (id, roleid, userid, create_time, creator)
values (100028, 2, 211, to_date('27-05-2017 13:44:30', 'dd-mm-yyyy hh24:mi:ss'), 0);
insert into SYS_ROLE_USER (id, roleid, userid, create_time, creator)
values (100029, 212, 211, to_date('27-05-2017 13:44:30', 'dd-mm-yyyy hh24:mi:ss'), 0);
commit;
prompt 4 records loaded
prompt Loading SYS_USER...
insert into SYS_USER (id, username, name, password, email, phone, state, create_time, creator)
values (0, 'dev', 'dev', '202CB962AC59075B964B07152D234B70', '大神', null, '0', to_date('23-03-2017 20:26:26', 'dd-mm-yyyy hh24:mi:ss'), 0);
insert into SYS_USER (id, username, name, password, email, phone, state, create_time, creator)
values (211, 'test', '测试人员1', '098F6BCD4621D373CADE4E832627B4F6', null, '111', '0', to_date('20-04-2017 16:34:06', 'dd-mm-yyyy hh24:mi:ss'), 0);
insert into SYS_USER (id, username, name, password, email, phone, state, create_time, creator)
values (2, 'admin', 'admin', '21232F297A57A5A743894A0E4A801FC3', null, null, '0', to_date('23-03-2017 20:26:41', 'dd-mm-yyyy hh24:mi:ss'), 0);
commit;
prompt 3 records loaded
set feedback on
set define on
prompt Done.

create sequence SEQ_ID
minvalue 1
maxvalue 9999999999999999999999999999
start with 100460
increment by 1
cache 20;
